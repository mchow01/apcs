�   bStringLinkedLibrary�� {Created By:		Ming Chow}�  {Created On:		November 22, 1996}��   ����SpacexZ ��AstriskxZ*����
StringTypej
StringNode��
StringNodel~	Character�~char�~Next�~
StringType�����
  .CreateString��~Str�~
StringType���
  bDeleteString��~First�~
StringType���  �Length�~Str�~
StringType��~integer��	  �Concatenate�~Str1|~Str2�~
StringType��~
StringType��	   
CopyString�~Str�~
StringType��~
StringType��  |Extract�~Str�~
StringType�~Start|~NumChar�~integer��~
StringType��
  �UpperToLower��~Str�~
StringType���  �UpperToLowerChar��~	Character�~char���
  LowerToUpper��~Str�~
StringType���  LLowerToUpperChar��~	Character�~char���  �Position�~Str�~
StringType�~Letter�~char��~integer��  �Replace�~OldStr�~
StringType�~OldChar|~NewChar�~char��~
StringType��	  @
AppendChar��~Str�~
StringType�~Letter�~char���	  pPrintString�~Str�~
StringType����  ؔ��/ X(**************************************************************************************)�
  JCreateString��~Str�~
StringType����~Letter�~char�~Mover|~Temp�~
StringType���     ��   {of Procedure CreateString}�\EOLNx\Str
X��      �x\New\Str�x	\read\Letter�x\StrTR\	Character
\Letter�x\Mover
\Str��$\EOLN�      rx	\read\Letter�x\New\Temp�x\MoverTR\Next
\Temp�x\TempTR\	Character
\Letter�x\Mover
\Temp���x
\MoverTR\Next
X���~readln����   {of Procedure CreateString}��/ X(**************************************************************************************)�
  ~DeleteString��~First�~
StringType����~Mover|~Temp�~
StringType���  �  6�   {of Procedure DeleteString}x	\Mover
\First��\FirstFX�      2x\First
\FirstTR\Next�x
\MoverTR\Next
X�x
\Dispose\Mover�x	\Mover
\First������   {of Procedure DeleteString}��/ X(**************************************************************************************)�  �Length�~Str�~
StringType��~integer���  	�  	^�   {of Function Length}�\StrDXx\Length
Z  �x\Length
Z <\Length\StrTR\Next����   {of Function Length}��/ X(**************************************************************************************)�	  
DConcatenate�~Str1|~Str2�~
StringType��~
StringType���~Mover�~
StringType���  �  �   {of Function Concatenate}�\Str1DXx\Concatenate
\Str2��      x\Mover
\Str1��\MoverTR\NextFXx\Mover
\MoverTR\Next�x\MoverTR\Next
\Str2�x\Concatenate
\Str1������   {of Function Concatenate}��/ X(**************************************************************************************)�	  
CopyString�~Str�~
StringType��~
StringType���~Mover|~	CopyMover|~TempCopy�~
StringType���  @  ��   {of Function CopyString}�\StrDXx\
CopyString
X��      �x
\New\TempCopy�x\TempCopyTR\	Character
\StrTR\	Character�x\Mover
\Str�x\	CopyMover
\TempCopy��\MoverTR\NextFX�      �x\New\	CopyMoverTR\Next�x\Mover
\MoverTR\Next�x\	CopyMover
\	CopyMoverTR\Next�x\	CopyMoverTR\	Character
\MoverTR\	Character���x\	CopyMoverTR\Next
X�x\
CopyString
\TempCopy������   {of Function CopyString}��/ X(**************************************************************************************)�  �Extract�~Str�~
StringType�~Start|~NumChar�~integer��~
StringType���~Mover|~Temp|~NewFirst|~NewOne�~
stringType�~Count�~integer���  ,  ��   {of Function Extract}x\Mover
\Str��CountZ \Start>Z x\Mover
\MoverTR\Next�x
\New\NewFirst�x\NewFirstTR\	Character
\MoverTR\	Character�x
\Temp
\NewFirst��CountZ \NumChar>Z �      ~x	\New\NewOne�x\TempTR\Next
\NewOne�x\Mover
\MoverTR\Next�x\Temp
\TempTR\Next�x\TempTR\	Character
\MoverTR\	Character���x	\TempTR\Next
X�x\Extract
\NewFirst����   {of Function Extract}��/ X(**************************************************************************************)�
  ~UpperToLower��~Str�~
StringType����~Mover�~
StringType���  �  Z�   {of Procedure UpperToLower}x\Mover
\Str��\MoverFX�      V�\MoverTR\	CharacterPZA,ZZx \MoverTR\	Character
\chr\ord\MoverTR\	Character<Z  �x\Mover
\MoverTR\Next������   {of Procedure UpperToLower}��/ X(**************************************************************************************)�  UpperToLowerChar��~	Character�~char����    ��    {of Procedure UpperToLowerChar}�\	CharacterPZA,ZZx\	Character
\chr\ord\	Character<Z  ����    {of Procedure UpperToLowerChar}��/ X(**************************************************************************************)�
  lLowerToUpper��~Str�~
StringType����~Mover�~
StringType���  �    �   {of Procedure LowerToUpper}x\Mover
\Str��\MoverFX�        �\MoverTR\	CharacterPZa,Zzx \MoverTR\	Character
\chr\ord\MoverTR\	Character>Z  �x\Mover
\MoverTR\Next������   {of Procedure LowerToUpper}��/ X(**************************************************************************************)�  LowerToUpperChar��~	Character�~char����    H�    {of Procedure LowerToUpperChar}�\	CharacterPZa,Zzx\	Character
\chr\ord\	Character>Z  ����    {of Procedure LowerToUpperChar}��/ X(**************************************************************************************)�    Position�~Str�~
StringType�~Letter�~char��~integer���~Mover�~
StringType�~Found�~boolean�~Count�~integer���      ��   {of Function Position}x\Count
Z �x\Mover
\Str�x	\Found
\False��\MoverFX8$\Found�      ^�\MoverTR\	CharacterD\Letterx\Found
\True��      Zx\Mover
\MoverTR\Next�x\Count
\Count<Z ������\Foundx
\Position
\Count�x	\Position
Z  ����   {of Function Position}��/ X(**************************************************************************************)�  �Replace�~OldStr�~
StringType�~OldChar|~NewChar�~char��~
StringType���~Mover�~
StringType���  �  `�   {of Procedure Replace}x	\Mover
\OldStr��\MoverFX�      F�\MoverTR\	CharacterD\OldCharx\MoverTR\	Character
\NewChar�x\Mover
\MoverTR\Next���x
\Replace
\OldStr����   {of Procedure Replace}��/ X(**************************************************************************************)�	  l
AppendChar��~Str�~
StringType�~Letter�~char����~Mover|~Temp�~
StringType�~Len|~Count�~integer���  �  j�   {of Procedure AppendChar}x\Count
Z  �x\Len
\Length\Str�x\Mover
\Str��CountZ \Len>Z x\Mover
\MoverTR\Next�x\New\Temp�x\TempTR\	Character
\Letter�x	\TempTR\Next
X�x\MoverTR\Next
\Temp����   {of Procedure AppendChar}��/ X(**************************************************************************************)�	  :PrintString�~Str�~
StringType����~Mover�~
StringType���      ܒ   {of Procedure PrintString}x\Mover
\Str��\MoverFX�      �x\write\MoverTR\	Character�x\Mover
\MoverTR\Next���~writeln����   {of Procedure PrintString}��/ X(**************************************************************************************)�� �   {of StringsLinkedLibrary}