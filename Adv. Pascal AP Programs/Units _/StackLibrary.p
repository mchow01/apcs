�
   �StackLibrary�� {Created By:				Ming Chow}� ${Date Created:			September 13, 1996}� #{Separate unit implementing stacks}��  ����	StackSizez  ����RealStackTypel~	StackList�`t	    	StackSized~real�~StackTop�~integer����CharStackTypel~	StackList�`t	    	StackSized~char�~StackTop�~integer�����~	RealStack�~RealStackType�~	CharStack�~CharStackType���  InitializeStack��~	RealStack�~RealStackType��~	CharStack�~CharStackType���	  N
IsEmptyNum�~	RealStack�~RealStackType��~boolean��	  �IsEmptyChar�~	CharStack�~CharStackType��~boolean��  �	IsFullNum�~	RealStack�~RealStackType��~boolean��	  
IsFullChar�~	CharStack�~CharStackType��~boolean��  `	PopNumber��~	RealStack�~RealStackType��~Number�~real���  �PopChar��~	CharStack�~CharStackType��~	Character�~char���	  �
PushNumber��~	RealStack�~RealStackType�~Number�~real���  LPushChar��~	CharStack�~CharStackType�~	Character�~char����  ���/ X(**************************************************************************************)�  ,InitializeStack��~	RealStack�~RealStackType��~	CharStack�~CharStackType����~Count�~integer���  �  B�   {of Procedure InitializeStack}x\	RealStackR\StackTop
Z  �x\	CharStackR\StackTop
Z  ��\	RealStack�      �CountZ \	StackSizex\	StackList\Count
Z  ����\	CharStack�      >�CountZ \	StackSizex\	StackList\Count
Z ������   {of Procedure InitializeStack}��/ X(**************************************************************************************)�	  
IsEmptyNum�~	RealStack�~RealStackType��~boolean���      d�   {of Function IsEmpty}x\
IsEmptyNum
\	RealStackR\StackTopDZ  ����   {of Function IsEmpty}��/ X(**************************************************************************************)�	  $IsEmptyChar�~	CharStack�~CharStackType��~boolean���  �  ~�   {of Function IsEmpty}x\IsEmptyChar
\	CharStackR\StackTopDZ  ����   {of Function IsEmpty}��/ X(**************************************************************************************)�  	<	IsFullNum�~	RealStack�~RealStackType��~boolean���  
  	��   {of Procedure IsFull}x\	IsFullNum
\	RealStackR\StackTopD\	StackSize����   {of Procedure IsFull}��/ X(**************************************************************************************)�	  
Z
IsFullChar�~	CharStack�~CharStackType��~boolean���  6  
��   {of Procedure IsFull}x\
IsFullChar
\	CharStackR\StackTopD\	StackSize����   {of Procedure IsFull}��/ X(**************************************************************************************)�  �	PopNumber��~	RealStack�~RealStackType��~Number�~real����  �  :�   {of Procedure PopNumber}� %{Precondition:				Stack is not empty}�\	RealStack�      6x\Number
\	StackList\StackTop�x\StackTop
\StackTop>Z ������   {of Procedure PopNumber}��/ X(**************************************************************************************)�  PopChar��~	CharStack�~CharStackType��~	Character�~char����  :  ��   {of Procedure PopChar}� %{Precondition:				Stack is not empty}�\	CharStack�      �x\	Character
\	StackList\StackTop�x\StackTop
\StackTop>Z ������   {of Procedure PopChar}��/ X(**************************************************************************************)�	  �
PushNumber��~	RealStack�~RealStackType�~Number�~real����  �  @�   {of Procedure PushNumber}� ${Precondition:				Stack is not full}�\	RealStack�      <x\StackTop
\StackTop<Z �x\	StackList\StackTop
\Number������   {of Procedure PushNumber}��/ X(**************************************************************************************)�  PushChar��~	CharStack�~CharStackType�~	Character�~char����      Ȓ   {of Procedure PushChar}� ${Precondition:				Stack is not full}�\	CharStack�      �x\StackTop
\StackTop<Z �x\	StackList\StackTop
\	Character������   {of Procedure PushChar}��/ X(**************************************************************************************)�� 