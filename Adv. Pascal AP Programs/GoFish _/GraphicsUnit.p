�
   tGraphicsUnit�� {Created By:			Ming Chow}� 9{Used in conjunction with "Macintosh Pascal Programming"}��  ���~CardUnit|~GameUnit����BASE_RES_IDz ���TitleID~BASE_RES_ID��AboutIDx
\BASE_RES_ID<Z ��	VictoryIDx
\BASE_RES_ID<Z ��	InstructionsIDx
\BASE_RES_ID<Z ���~	gPictureWindow�~	WindowPtr��� {Procedures and Functions}�	  �
CenterPict�~
thePicture�~	PicHandle��~myRect�~Rect���
  DrawMyPicture�~pictureWindow�~	WindowPtr�~PictID�~integer���	  0
WindowInit��
  `gPrintString�~Str�~
StringType���  �SetColor�~Color�~integer���
  �gDisplayHand�~Hand�~HandPointer�~Name�~
StringType���
  
gDisplayPairs�~Pairs�~HandPointer����  r���/ X(**************************************************************************************)�	  �
CenterPict�~
thePicture�~	PicHandle��~myRect�~Rect����~windRect|~pictureRect�~Rect���      ��   {of Procedure CenterPict}x\windRect
\myRect�x\pictureRect
\
thePictureTTR\picFrame�x>\myRectR\top
\windRectR\bottom>\windRectR\top>\pictureRectR\bottom>\pictureRectR\top4Z <\windRectR\top�x(\myRectR\bottom
\myRectR\top<\pictureRectR\bottom>\pictureRectR\top�x>\myRectR\Left
\windRectR\right>\windRectR\Left>\pictureRectR\right>\pictureRectR\Left4Z <\windRectR\Left�x(\myRectR\right
\myRectR\Left<\pictureRectR\right>\pictureRectR\Left����   {of Procedure CenterPict}��/ X(**************************************************************************************)�
  �DrawMyPicture�~pictureWindow�~	WindowPtr�~PictID�~integer����~myRect�~Rect�~
thePicture�~	PicHandle���  .  Ԓ   {of Procedure DrawPicture}x\myRect
\pictureWindowTR\portRect�x\
thePicture
\
GetPicture\PictID�x\
CenterPict\
thePicture\myRect�x\DrawPicture\
thePicture\myRect����   {of Procedure DrawPicture}��/ X(**************************************************************************************)�	  D
WindowInit���  	|  	"�   {of Procedure WindowInit}x#\gPictureWindow
\GetNewWindow\BASE_RES_IDX\	WindowPtr^Z �x\
ShowWindow\gPictureWindow�x\SetPort\gPictureWindow����   {of Procedure WindowInit}��/ X(**************************************************************************************)�
  	�gPrintString�~Str�~
StringType����~Mover�~
StringType���  
�  
��   {of Procedure PrintString}x\Mover
\Str��\MoverFX�      
�x\	WriteDraw\MoverTR\	Character�x\Mover
\MoverTR\Next������   {of Procedure PrintString}��/ X(**************************************************************************************)�  xSetColor�~Color�~integer����~red�~RGBColor�~yellow�~RGBColor�~blue�~RGBColor�~white�~RGBColor���    ��   {of Procedure SetColor}�\Colordz  ��      Px\yellowR\red
^Z �x\yellowR\green
^Z �x\yellowR\blue
Z �x\RGBforecolor\yellow���z  ��      �x
\redR\red
^Z �x
\redR\green
Z �x
\redR\blue
Z �x\RGBforecolor\red���z  ��      8x
\blueR\red
Z �x\blueR\green
Z �x\blueR\blue
^Z �x\RGBforecolor\blue���z  ��      �x\whiteR\red
^Z �x\whiteR\green
^Z �x\whiteR\blue
^Z �x\RGBforecolor\white��������   {of Procedure SetColor}��/ X(**************************************************************************************)�
  �gDisplayHand�~Hand�~HandPointer�~Name�~
StringType����~Mover�~HandPointer�~FontNum|~Top|~Left|~Bottom|~Right|~Val|~Count�~integer���  J  �   {of Procedure gDisplayHand}~
WindowInit�x\MoveToZ Z �x\	WriteDrawZCURRENT PLAYER ---> �x\gPrintString\Name�x\MoveToZ Z �x\	WriteDrawZCurrent playerZ'Zs hand:�x\MoveToZ Z ��x\	WriteDrawZCurrent playerZ'Zs pairs:�x\MoveToZ Z ��x\	WriteDrawZ)PRESS THE << MOUSE BUTTON >> TO CONTINUE:�x
\SetColorZ �x\Val
\	CountHand\Hand�x\Top
Z -�x\Left
Z �x\Bottom
Z i�x\Right
Z 2��	CountZ \Val�      �x\	PaintRect\Top\Left\Bottom\Right�x"\
InvertRect\Top<Z \Left<Z \Bottom>Z \Right>Z �x
\Left
\Left<Z 7�x\Right
\Right<Z 7���x\Left
Z �x\Right
Z A�x\Count
Z �x\Mover
\Hand��	CountZ \Val�      x\MoveTo\Left\Right��\MoverTR\CardR\CardValdz  �x
\	WriteDrawZA�z  �x
\	WriteDrawZ2�z  �x
\	WriteDrawZ3�z  �x
\	WriteDrawZ4�z  �x
\	WriteDrawZ5�z  �x
\	WriteDrawZ6�z  �x
\	WriteDrawZ7�z  �x
\	WriteDrawZ8�z  	�x
\	WriteDrawZ9�z  
�x
\	WriteDrawZ10�z  �x
\	WriteDrawZJ�z  �x
\	WriteDrawZQ�z  �x
\	WriteDrawZK���x\Mover
\MoverTR\Next�x
\Left
\Left<Z 7���x\GetFNumZ
VegasPoker\FontNum�x\TextFont\FontNum�x
\TextSizeZ �x\Left
Z �x\Right
Z U�x\Count
Z �x\Mover
\Hand��	CountZ \Val�      �x\MoveTo\Left\Right��\MoverTR\CardR\Kindd~Spades�x
\	WriteDrawZA�~Hearts�x
\	WriteDrawZB�~Clubs�x
\	WriteDrawZD�~Diamonds�x
\	WriteDrawZC���x\Mover
\MoverTR\Next�x
\Left
\Left<Z 7���x\FontNum
Z �x\TextFont\FontNum����   {of Procedure gDisplayHand}��/ X(**************************************************************************************)�
  �gDisplayPairs�~Pairs�~HandPointer����~Mover�~HandPointer�~FontNum|~Top|~Left|~Bottom|~Right|~Val|~Count�~integer���       ��   {of Procedure DisplayPairs}x\FontNum
Z �x
\TextSizeZ �x\TextFont\FontNum�x\Val
\
CountPairs\Pairs�x\Top
Z ��x\Left
Z �x\Bottom
Z Șx\Right
Z 2��	CountZ \Val�      �x\	PaintRect\Top\Left\Bottom\Right�x"\
InvertRect\Top<Z \Left<Z \Bottom>Z \Right>Z �x
\Left
\Left<Z 7�x\Right
\Right<Z 7���x\Left
Z �x\Right
Z ��x\Count
Z �x	\Mover
\Pairs��	CountZ \Val�      x\MoveTo\Left\Right��\MoverTR\CardR\CardValdz  �x
\	WriteDrawZA�z  �x
\	WriteDrawZ2�z  �x
\	WriteDrawZ3�z  �x
\	WriteDrawZ4�z  �x
\	WriteDrawZ5�z  �x
\	WriteDrawZ6�z  �x
\	WriteDrawZ7�z  �x
\	WriteDrawZ8�z  	�x
\	WriteDrawZ9�z  
�x
\	WriteDrawZ10�z  �x
\	WriteDrawZJ�z  �x
\	WriteDrawZQ�z  �x
\	WriteDrawZK���x\Mover
\MoverTR\Next�x\Mover
\MoverTR\Next�x
\Left
\Left<Z 7���x\Top
Z ��x\Left
Z �x\Bottom
Z �x\Right
Z 2�x\Count
Z �x	\Mover
\Pairs��	CountZ \Val�      @x\	PaintRect\Top\Left\Bottom\Right�x"\
InvertRect\Top<Z \Left<Z \Bottom>Z \Right>Z �x
\Left
\Left<Z 7�x\Right
\Right<Z 7���x\Left
Z �x\Right
Z ��x\Count
Z �x	\Mover
\Pairs��	CountZ \Val�      �x\MoveTo\Left\Right��\MoverTR\CardR\CardValdz  �x
\	WriteDrawZA�z  �x
\	WriteDrawZ2�z  �x
\	WriteDrawZ3�z  �x
\	WriteDrawZ4�z  �x
\	WriteDrawZ5�z  �x
\	WriteDrawZ6�z  �x
\	WriteDrawZ7�z  �x
\	WriteDrawZ8�z  	�x
\	WriteDrawZ9�z  
�x
\	WriteDrawZ10�z  �x
\	WriteDrawZJ�z  �x
\	WriteDrawZQ�z  �x
\	WriteDrawZK���x\Mover
\MoverTR\Next�x\Mover
\MoverTR\Next�x
\Left
\Left<Z 7���x\GetFNumZ
VegasPoker\FontNum�x\TextFont\FontNum�x
\TextSizeZ �x\Left
Z �x\Right
Z Ҙx\Count
Z �x	\Mover
\Pairs��	CountZ \Val�       �x\MoveTo\Left\Right��\MoverTR\CardR\Kindd~Spades�x
\	WriteDrawZA�~Hearts�x
\	WriteDrawZB�~Clubs�x
\	WriteDrawZD�~Diamonds�x
\	WriteDrawZC���x\Mover
\MoverTR\Next�x\Mover
\MoverTR\Next�x
\Left
\Left<Z 7���x\FontNum
Z �x\TextFont\FontNum����   {of Procedure DisplayPairs}��/ X(**************************************************************************************)�� �   {of GraphicsUnit}