# Overview
This is an archive of computer programs I wrote in Advanced Placement Computer Science classes at Saugus High School in Saugus, MA.

* `Pascal AP Programs` - circa 1996, sophomore year in high school. The course was "Pascal AP"
* `Adv. Pascal AP Programs` - circa 1997, junior year in high school. The course was "Advanced Pascal AP"

# What programming language was used?
Pascal

# What was used to write the programs back in the days?
* Hardware: Apple Macintosh
* Operating System: System 7.x
* Compiler: THINK Pascal